#!/bin/bash
# require JQ, run 'sudo apt install jq'

VERSION=$1
if [ -z $VERSION ]; then
  VERSION=$(curl -sL  -u "$GH_USER:$GH_API" https://api.github.com/repos/home-assistant/hassbian-scripts/tags | jq -r .[0].name | awk -F'v' '{print $NF}')
fi
MILESTONES=$(curl -sL -u "$GH_USER:$GH_API" https://api.github.com/search/issues\?q\=milestone:v$VERSION+type:pr+repo:home-assistant/hassbian-scripts | jq -r '.items[]| "[#" +(.number|tostring) +"](" + .html_url + ") " + .title + " [@" + (.user | .login + "](" + .html_url + ")  ")')
if [ ! "$MILESTONES" ]; then
        exit 1
fi

if [ -f hassbian_config_$VERSION.md ];then
  rm hassbian_config_$VERSION.md
fi
echo "**$VERSION**  "
echo "$MILESTONES"
