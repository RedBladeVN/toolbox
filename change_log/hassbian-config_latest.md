**0.9.1**  
[#166](https://github.com/home-assistant/hassbian-scripts/pull/166) homeassistant: Version check for beta [@ludeeus](https://github.com/ludeeus)  
[#165](https://github.com/home-assistant/hassbian-scripts/pull/165) homeassistant: fixes after changes on pypi. [@ludeeus](https://github.com/ludeeus)  
