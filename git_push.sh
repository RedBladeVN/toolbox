#!/bin/bash
configdir="/dockerfiles"
timevar=$(date +"%Y.%m.%d %H:%M:%S")
commitmsg="Automatic commit $timevar"
cd $configdir
git add .
git commit -m "$commitmsg"
git push origin master