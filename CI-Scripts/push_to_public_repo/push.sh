#!/bin/bash
#Author: Joakim Sørensen @ludeeus
#
# usage: push.sh [TZ]
# example: bash push.sh Europe/Oslo

timevar=$(TZ=$1 date +"%Y.%m.%d %H:%M:%S")
message="Automated commit $timevar"

if [ -f ./init.sh ]; then
    rm ./init.sh
fi

if [ -f ./push.sh ]; then
    rm ./push.sh
fi

cd /publish

git add .
git commit -m "$message" || exit 0
git push -u origin master || exit 1

echo "push.sh finished.."