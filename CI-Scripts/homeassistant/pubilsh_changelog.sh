#!/bin/bash
# Author: Joakim Sørensen @ludeeus
# Helper script to build and distribute hassbian-scripts.

mkdir /publish

git config --global user.name "$SH_USER" || exit 1
git config --global user.email "$SH_MAIL" || exit 1

echo "creating releasenotes..."
cd /publish || exit 1
git init
git remote add origin https://${SH_USER}:${SH_API}@gitlab.com/ludeeus/toolbox.git || exit 1
git fetch --all && git reset --hard origin/master

curl -sL https://gitlab.com/ludeeus/toolbox/raw/master/generate_changelog/homeassistant_breaking_changes.sh | bash | tee /publish/change_log/breaking_changes_home_assistant_latest.md

latest=$(cat /publish/change_log/breaking_changes_home_assistant_latest.md | head -1)
history=$(cat /publish/change_log/breaking_changes_home_assistant_history.md | head -1)

if [ "$latest" != "$history" ]; then
  mv /publish/change_log/breaking_changes_home_assistant_history.md /publish/change_log/breaking_changes_home_assistant_history.md_backup
  cat /publish/change_log/breaking_changes_home_assistant_latest.md | tee -a /publish/change_log/breaking_changes_home_assistant_history.md
  echo "" | tee -a /publish/change_log/breaking_changes_home_assistant_history.md
  cat /publish/change_log/breaking_changes_home_assistant_history.md_backup | tee -a /publish/change_log/breaking_changes_home_assistant_history.md
  rm /publish/change_log/breaking_changes_home_assistant_history.md_backup
fi

git add -A
git commit -a -m "Updated breaking changes for Home Assistant."
git push -u origin master

echo "Complete..."