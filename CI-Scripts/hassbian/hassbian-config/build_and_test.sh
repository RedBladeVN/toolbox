#!/bin/bash
# Author: Joakim Sørensen @ludeeus
# Helper script to build and distribute hassbian-scripts.

branch=$1

if [ "$branch" == "dev"  ]; then
    timestamp=$(date +%s)
    CURRENTVERSION=$(cat ${CI_PROJECT_DIR}/package/DEBIAN/control | grep 'Version' | awk -F ' ' '{print $2}')
    VERSION=$(echo "${CURRENTVERSION}-dev-${timestamp}")
    sed -i 's/'$CURRENTVERSION'/'$VERSION'/g' ${CI_PROJECT_DIR}/package/DEBIAN/control
fi

PACKAGE_VERSION=$(cat ${CI_PROJECT_DIR}/package/DEBIAN/control | grep 'Version' | awk -F ' ' '{print $2}')

echo $PACKAGE_VERSION | tee /build/version

chmod 755 -R ${CI_PROJECT_DIR}
dpkg-deb --build package/

if [ -f  ${CI_PROJECT_DIR}/package.deb ] ; then
    exit 0
else
    exit 1
fi