#!/bin/bash
# Author: Joakim Sørensen @ludeeus
# Helper script to build and distribute hassbian-scripts.

mkdir /publish

git config --global user.name "$SH_USER" || exit 1
git config --global user.email "$SH_MAIL" || exit 1

echo "creating releasenotes..."
cd /publish || exit 1
git init
git remote add origin https://${SH_USER}:${SH_API}@gitlab.com/ludeeus/toolbox.git || exit 1
git fetch --all && git reset --hard origin/master

curl -sL https://gitlab.com/ludeeus/toolbox/raw/master/generate_changelog/hassbian-config.sh | bash | tee /publish/change_log/hassbian-config_latest.md

git add -A
git commit -a -m "New releasenotes for hassbian-config."
git push -u origin master

echo "Complete..."