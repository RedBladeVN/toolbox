#!/bin/bash
#Author: Joakim Sørensen @ludeeus

# Workaround for broken DHT sensor in 0.73.0
# execute with: 
# curl -sSL https://gitlab.com/ludeeus/toolbox/raw/master/homeassistant/workarounds/0.73.0%20-%20sensor.dht-hassbian.sh | sudo bash

# Only tested on Hassbian!

sudo -u homeassistant -H /bin/bash << EOF
wget -O /srv/homeassistant/lib/python3.5/site-packages/homeassistant/components/sensor/dht.py https://raw.githubusercontent.com/home-assistant/home-assistant/e76e9e0966b01dd16e8e5205a3825b6e8d30ce39/homeassistant/components/sensor/dht.py
EOF
systemctl restart home-assistant@homeassistant.service

echo "done"