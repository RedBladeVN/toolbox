# Docker host
Debian 9
Added dir `/dockerfiles/` to store all container data.
Installed `samba` to expose that dir.
***

## ha-dockermon
[DockerHub](https://hub.docker.com/r/philhawthorne/ha-dockermon)
```bash
docker run -d --name=ha-dockermon --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /dockerfiles/hadocermon:/config -p 8126:8126 philhawthorne/ha-dockermon
```
***

## mosquitto
[DockerHub](https://hub.docker.com/_/eclipse-mosquitto/)
```bash
docker run -it -p 1883:1883 -p 9001:9001 -v /dockerfiles/mosquitto/mosquitto.conf:/mosquitto/config/mosquitto.conf:rw eclipse-mosquitto
```
***

## portainerer
[DockerHub](https://hub.docker.com/r/portainer/portainer)
```bash
docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /dockerfiles/portainer-data/:/data portainer/portainer
```
***

## watchtower
[GitHub](https://github.com/v2tec/watchtower)\
[DockerHub](https://hub.docker.com/r/v2tec/watchtower/)

```bash
docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock v2tec/watchtower
```
***